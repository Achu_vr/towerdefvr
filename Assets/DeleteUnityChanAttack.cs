﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteUnityChanAttack : MonoBehaviour {

	private GameObject[] atkObj;
	public bool canInstantiate = true;

	void Update () {

		atkObj = GameObject.FindGameObjectsWithTag ("UnityChanAttack");

		if (atkObj.Length > 0)
			canInstantiate = false;
		else
			canInstantiate = true;

		if (atkObj.Length >= 2) {
			for (int i = 1; i < atkObj.Length; i++) {
				Destroy (atkObj [i]);
			}
		}
	}
}
