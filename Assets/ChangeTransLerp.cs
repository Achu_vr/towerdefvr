﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTransLerp : MonoBehaviour {

	[SerializeField]private GameObject pointObj;
	[SerializeField]private GameObject playerPos;

	void Update () {
		transform.LookAt (playerPos.transform);
		this.transform.position = Vector3.Lerp (this.transform.position, pointObj.transform.position, 0.04f);
	}
}
