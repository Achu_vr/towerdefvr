﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForExplaine : MonoBehaviour {

	[SerializeField]private GameObject canvasExplane;

	// Use this for initialization
	void Start () {
		StartCoroutine ("WaitForCanvas");
	}

	IEnumerator WaitForCanvas() {
		yield return new WaitForSeconds (1f);
		canvasExplane.SetActive (true);

	}

}
