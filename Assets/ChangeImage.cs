﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeImage : MonoBehaviour {

	[SerializeField]private Sprite image1;
	[SerializeField]private Sprite image2;
	[SerializeField]private float maxTime;
	private float timer = 0;
	private bool whichImage = true;

	private SpriteRenderer ren;

	void Start() {
		ren = GetComponent<SpriteRenderer> ();
	}

	void Update () {
		timer += Time.deltaTime;
		if (timer >= maxTime && whichImage) {
			whichImage = false;
			ren.sprite = image2;
			timer = 0;
		} else if(timer >= maxTime && !whichImage) {
			ren.sprite = image1;
			whichImage = true;
			timer = 0;
		}
	}
}
