﻿using UnityEngine;

public class DestroyTime : MonoBehaviour {

	[SerializeField]private float maxTimeForDestroy;

	void Start () {
		Destroy (this.gameObject, maxTimeForDestroy);	
	}

}
