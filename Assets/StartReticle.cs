﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartReticle : MonoBehaviour {

	[SerializeField]private GameStartButton gameStartButton;
	private float timer = 0;
	[SerializeField]private Image reticle;
	[SerializeField]private float speed;

	void Update () {
		timer = gameStartButton.timer;
		reticle.fillAmount = speed * timer;
	}
}
