﻿using UnityEngine;
using UnityEngine.UI;

public class reticleManager : MonoBehaviour {

	[SerializeField]private CameraRay cameraRay;
	private float timer = 0;
	[SerializeField]private Image reticle;
	[SerializeField]private float speed;

	void Update () {
		timer = cameraRay.timer;
		reticle.fillAmount = timer * speed;
	}
}
