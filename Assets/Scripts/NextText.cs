﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextText : MonoBehaviour {

	public static bool canReadNextMessage = false;
	public static string nextMessage = "";
	[SerializeField]private TextController textController;
	[SerializeField]private GameObject Stage1;
	[SerializeField]private GameObject Tiles;
	[SerializeField]private GameObject StartParticle;
	[SerializeField]private GameObject EndParticle;
	[SerializeField]private GameObject Skeltons;
	[SerializeField]private GameObject StartButton;

	void Update() {
		switch(textController.currentText) {

		case "↓これがボードです↓":
			Stage1.SetActive (true);		
			break;

		case "↓ここにコマをおいていきます↓\n":
			Tiles.SetActive (true);
			break;

		case "ボード右側から敵が出現します↓\n":
			Tiles.SetActive (false);
			StartParticle.SetActive (true);
			Skeltons.SetActive (true);
			break;

		case "↓敵がボード左側まで来てしまっ\nたらゲームオーバーです":
			EndParticle.SetActive (true);
			break;

		case "一度ボードの中へ入ってみましょ\nう":
			StartButton.SetActive (true);
			break;
		}
	}

}
