﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftVibration : MonoBehaviour {

	SteamVR_TrackedObject trackedObject;
	SteamVR_Controller.Device device;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
	}


	public void ControllerVibration(float time) {
		StartCoroutine (Pulse (time));
	}

	private IEnumerator Pulse(float time) {
		float timer = 0;
		while (time >= timer) {
			timer += Time.deltaTime;
			device.TriggerHapticPulse (1000);
			yield return null;
		}
	}
}
