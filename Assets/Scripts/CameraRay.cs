﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRay : MonoBehaviour {

	Ray ray;
	RaycastHit hit;
	public float timer = 0;
	[SerializeField]private GameObject center;

	void Start () {
	}

	void Update () {
		ray = new Ray (this.transform.position, center.transform.position);
		Debug.DrawRay (this.transform.position, center.transform.position, Color.green);
		if (Physics.Raycast (ray, out hit)) {
			FindObject (hit.collider);		
		} else {
			timer = 0;
		}
	}

	void FindObject(Collider col) {
		if (col.tag == "text") {
			timer += Time.deltaTime;
		} else {
			timer = 0;
		}

		if (timer >= 3.37f) {
			timer = 0;
			NextText.canReadNextMessage = true;
		}

	}

}
