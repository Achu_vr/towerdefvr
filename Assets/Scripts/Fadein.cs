﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fadein : MonoBehaviour {

	[SerializeField]private SpriteRenderer fadeImage;
	[SerializeField]private float duration = 2.8f;
	public bool isFinished = false;

	[SerializeField]private Fadeout fadeoutScript;

	void Start () {
		StartCoroutine (FadeImageStart ());
	}

	public void StartFadeImage() {
		if (fadeoutScript.enabled == true) {
			fadeoutScript.enabled = false;
		} else if(fadeoutScript.enabled == true) {
			fadeoutScript.enabled = false;
		}
		StartCoroutine (FadeImage ());
	}

	IEnumerator FadeImage() {
		float timer = 0;
		float alpha = 1f;
		while (fadeImage.color.a >= 0) {
			timer += Time.deltaTime;
			alpha -= 0.9f * Time.deltaTime;
			fadeImage.color = new Color (0, 0, 0.1f, alpha);
			yield return null;
		}
		isFinished = true;
	}

	IEnumerator FadeImageStart() {
		float timer = 0;
		float alpha = 1f;
		while (fadeImage.color.a >= 0) {
			timer += Time.deltaTime;
			alpha -= 0.25f * Time.deltaTime;
			fadeImage.color = new Color (0, 0, 0, alpha);
			yield return null;
		}
	}
}
