﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTimer : MonoBehaviour {

	private float timer = 0;
	private bool canDisplay = false;
	[SerializeField]private MeshRenderer meshRenderer;
	[SerializeField]private TextController textController;

	void Update () {
		timer += Time.deltaTime;
		if (timer >= 3f && canDisplay == false) {
			timer = 0;
			textController.enabled = false;
			meshRenderer.enabled = false;
			canDisplay = true;
		} else if(timer >= 1f && canDisplay == true) {
			textController.enabled = true;
			meshRenderer.enabled = true;
			canDisplay = false;
		}

	}
}
