﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {
	public string[] scenarios;
	[SerializeField] TextMesh uiText;
	[SerializeField][Range(0.001f, 0.3f)]float intervalForCharacterDisplay = 0.05f;
	private int currentLine = 0;
	public string currentText = string.Empty;
	private float timeUntilDisplay = 0;
	private float timeElapsed = 1;
	private int lastUpdateCharacter = -1;
	[SerializeField]private string saveString = "";
	private bool doInsert = true;

	void Start () {
		SetNextLine ();
	}

	void Update () {

		if (currentLine < scenarios.Length && NextText.canReadNextMessage) {
			NextText.canReadNextMessage = false;
			SetNextLine ();
		}
		if(currentText.Length >= 15 && doInsert) {
			string secondSave = "";
			doInsert = false;
			saveString = currentText.Substring (0, 15);
			saveString += "\n";
			secondSave = currentText.Substring (15);
			saveString += secondSave;
			currentText = saveString;
		}

		int displayCharacterCount = (int)(Mathf.Clamp01 ((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
		if (displayCharacterCount != lastUpdateCharacter) {
			uiText.text = currentText.Substring (0, displayCharacterCount);
			lastUpdateCharacter = displayCharacterCount;
		}
	}

	void SetNextLine() {
		currentText = scenarios [currentLine];
		currentLine++;
		doInsert = true;
		timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
		timeElapsed = Time.time;

		lastUpdateCharacter = -1;
	}
}
