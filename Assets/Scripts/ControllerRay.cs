﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerRay : MonoBehaviour {

	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObject;
	[SerializeField]private GameObject cameraRig;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
	}

	void Update () {
		if(device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad)) {
			cameraRig.transform.position = SteamVR_LaserPointer.moveTrans;
		}
	}
}
