﻿Shader "Custom/UnityChanAttack" {
	Properties {
		_DiffuseColor("Diffuse Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_EmissionColor("Emmision Color", Color) = (0.0 ,0.0 ,0.0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM

		#pragma surface surf Lambert vertex:vert

		struct Input {
			float4 color : COLOR;
		};

		float4 _DiffuseColor;
		float4 _EmissionColor;

		void vert(inout appdata_full v) {
			v.vertex.x += 0.05 * v.normal.x * sin((v.vertex.y + _Time.x * 6) * 3.14 * 16);
			v.vertex.z += 0.05 * v.normal.z * sin((v.vertex.y + _Time.x * 6) * 3.14 * 16);
		}

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = _DiffuseColor;
			o.Emission = _EmissionColor;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
