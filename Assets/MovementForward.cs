﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementForward : MonoBehaviour {

	[SerializeField]private float speed;
	public static bool canShotHand = true;

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, 3.65f);
		StartCoroutine ("WaitThreeSeconds");
	}

	IEnumerator WaitThreeSeconds() {
		yield return new WaitForSeconds (3f);
		CanInstantiateHand ();
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position += transform.forward * Time.deltaTime * speed;
	}

	void CanInstantiateHand() {
		canShotHand = true;
	}
}
