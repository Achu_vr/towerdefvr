﻿using System.Collections;
using UnityEngine;

public class GetHandAndChangeCamera : MonoBehaviour {

	[SerializeField]private GameObject standardCamera;
	[SerializeField]private GameObject unityChanCamera;
	[SerializeField]private GameObject fadeImageFirst;
	[SerializeField]private GameObject fadeImageSecond;

	[SerializeField]private GameObject unityChanMesh1;
	[SerializeField]private GameObject unityChanMesh2;

	[SerializeField]private GameObject allCameras;

	[SerializeField]private GameObject[] controllers = new GameObject[2];

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "possessionHand" && this.gameObject.tag == "figure") {
			StartCoroutine ("FadeOut");
		}
	}

	IEnumerator FadeOut() {
		Fadeout fadeout = fadeImageFirst.GetComponent<Fadeout> ();
		fadeout.StartFadeImage ();

		while (!fadeout.isFinished) {
			yield return null;
		}

		fadeout.isFinished = false;

		standardCamera.SetActive (false);
		unityChanCamera.SetActive (true);
		allCameras.transform.position = transform.position;
		StartCoroutine ("FadeIn");
	}

	IEnumerator FadeIn() {
		Fadein fadein = fadeImageSecond.GetComponent<Fadein> ();
		fadein.StartFadeImage ();

		unityChanMesh1.SetActive (false);
		unityChanMesh2.SetActive (false);

		while(!fadein.isFinished) {
			yield return null;
		}


		controllers [0].SetActive (true);
		controllers [1].SetActive (true);
		fadein.isFinished = false;

	}


}
