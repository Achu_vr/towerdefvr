﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchpadTrans : MonoBehaviour {

	private GameObject touchpad;

	void Start () {
		touchpad = GameObject.Find ("Controller (right)");
	}

	void Update () {
		this.transform.position = touchpad.transform.position;
	}
}
