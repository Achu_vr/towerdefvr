﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotHand : MonoBehaviour {

	[SerializeField]private GameObject hand;

	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObject;
	[SerializeField]private RightVibration rightVib;

	public int shotCount = 0;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
	}

	void Update () {
		if (device.GetPressDown (SteamVR_Controller.ButtonMask.Trigger) && MovementForward.canShotHand) {
			MovementForward.canShotHand = false;
			rightVib.ControllerVibration (0.3f);
			Instantiate (hand, transform.position, transform.rotation);
			shotCount++;
		}
	}
}
