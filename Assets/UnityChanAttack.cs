﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanAttack : MonoBehaviour {

	private float xScale;
	private float yScale;
	private float zScale;

	private bool canMove = false;

	void Start () {

		xScale = transform.localScale.x;
		yScale = transform.localScale.y;
		zScale = transform.localScale.z;

		StartCoroutine ("ScaleChange");
	}

	void Update () {


		transform.position += transform.forward * Time.deltaTime;

		if (canMove) {
			this.gameObject.tag = "Untagged";
			this.GetComponent<MovementForward> ().enabled = true;
			this.transform.parent = null;
		}

	}

	IEnumerator ScaleChange() {

		bool canFinish = false;

		while (!canFinish) {

			this.transform.localScale = new Vector3 (xScale, yScale, zScale);
			xScale += 0.02f;
			yScale += 0.02f;
			zScale += 0.02f;

			if (xScale >= 0.8f) {
				canFinish = true;
			}

			yield return null;
		}

		canMove = true;

	}

}
