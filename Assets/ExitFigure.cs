﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitFigure : MonoBehaviour {

	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObject;
	[SerializeField]private LeftVibration leftVib;
	[SerializeField]private RightVibration rightVib;
	[SerializeField]private Fadeout fadeout;
	[SerializeField]private Fadein fadein;
	[SerializeField]private Transform respawnPoint;

	[SerializeField]private GameObject allCameras;
	[SerializeField]private GameObject mainCamera;
	[SerializeField]private GameObject unityChanCamera;
	[SerializeField]private GameObject unityChanMesh1;
	[SerializeField]private GameObject unityChanMesh2;
	[SerializeField]private GameObject fadeImage1;
	[SerializeField]private GameObject fadeImage2;
	[SerializeField]private GameObject unityChan;
	[SerializeField]private GameObject[] controllers = new GameObject[2];
	[SerializeField]private CountingMove cMove;

	public static bool canPressOneTime = false;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
		cMove.canLastStep = true;
		fadeImage2.GetComponent<Fadeout> ().enabled = true;
		fadeImage2.GetComponent<Fadein> ().enabled = true;
	}

	void Update () {

		if (!canPressOneTime) {
			if (device.GetPress (SteamVR_Controller.ButtonMask.ApplicationMenu)) {
				leftVib.ControllerVibration (3f);
				rightVib.ControllerVibration (3f);
				canPressOneTime = true;
				StartCoroutine (StartFadeout ());
			}
		}

	}

	IEnumerator StartFadeout() {
		fadeImage1.GetComponent<Fadein> ().enabled = true;
		fadeout.StartFadeImage ();
		while (!fadeout.isFinished) {
			yield return null;
		}

		fadeout.isFinished = false;
		unityChan.SetActive (false);

		mainCamera.SetActive (true);
		allCameras.transform.position = respawnPoint.position;
		mainCamera.transform.localPosition = Vector3.zero;
		fadeImage2.GetComponent<Fadeout> ().enabled = true;
		StartCoroutine ("FadeIn");
	}

	IEnumerator FadeIn() {
		fadein.StartFadeImage ();

		while(!fadein.isFinished) {
			yield return null;
		}
		fadein.isFinished = false;
		unityChanMesh1.SetActive (true);
		unityChanMesh2.SetActive (true);
		unityChan.SetActive (true);
		controllers [0].SetActive (true);
		controllers [1].SetActive (true);
		canPressOneTime = false;
		unityChanCamera.SetActive (false);
	}

}
