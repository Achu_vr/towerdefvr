﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTransform : MonoBehaviour {

	[SerializeField]private Transform playerTrans;
	[SerializeField]private Transform archerTrans;
	private Vector3 arrowPos;

	void Start () {
		
	}

	void Update () {
		arrowPos = new Vector3 (playerTrans.position.x, playerTrans.position.y + 0.3f, playerTrans.position.z);
		this.transform.LookAt (archerTrans);
		this.transform.position = arrowPos;
	}
}
