﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStartButton : MonoBehaviour {

	public static string stageNum = "Stage1";
	private Vector3 firstTrans;
	private Vector3 pushedTrans;
	[SerializeField]private RightVibration rightVibration;
	[SerializeField]private LeftVibration leftVibration;
	private AsyncOperation async;
	[SerializeField]private bool touched = false;
	[SerializeField] private bool didFiveSecondTouched = false;
	public float timer = 0;
	[SerializeField]private SpriteRenderer fadeImage;
	private bool canMove = false;

	void Start () {
		firstTrans = transform.position;		
		float y = firstTrans.y - 0.005f;
		pushedTrans = new Vector3 (firstTrans.x, y, firstTrans.z);
	}

	void Update () {
		if (canMove)
			async.allowSceneActivation = true;
	}

	void OnTriggerEnter(Collider other) {
		
		if (other.tag == "RightHand" || other.tag == "LeftHand") {
			this.transform.position = pushedTrans;
		}

		if (other.tag == "RightHand") {
			rightVibration.ControllerVibration (0.1f);
		}
		if (other.tag == "LeftHand") {
			leftVibration.ControllerVibration (0.1f);
		}
	}
	void OnTriggerStay(Collider other) {
		if (other.tag == "RightHand" || other.tag == "LeftHand") {
			timer += Time.deltaTime;
		}

		if (timer >= 5f) {
			didFiveSecondTouched = true;
		}
	}

	void OnTriggerExit(Collider other) {
		if (didFiveSecondTouched) {
			if (other.tag == "RightHand" || other.tag == "LeftHand") {
				StartCoroutine (LoadScene ());
				StartCoroutine (StartFaid ());
			}
		} else {
			timer = 0;
		}
		this.transform.position = firstTrans;
	}

	IEnumerator LoadScene() {
		if (!touched) {
			touched = true;
			async = SceneManager.LoadSceneAsync (stageNum);
			async.allowSceneActivation = false;
			while (!async.isDone) {
				Debug.Log (async.progress * 100 + "%");
				if (async.progress >= 0.9f) {
					break;
				}
				yield return null;
			}
			yield return async;
		}
	}

	IEnumerator StartFaid() {
		float duration = 2f;
		float timer = 0;
		float alpha = 0;

		while (fadeImage.color.a >= 1f) {
			timer += Time.deltaTime;
			fadeImage.color = new Color (0, 0, 0, alpha);
			alpha += 0.2f * Time.deltaTime;
			yield return null;
		}
		canMove = true;
	}
}
