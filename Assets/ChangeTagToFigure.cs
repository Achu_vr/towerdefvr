﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTagToFigure : MonoBehaviour {

	[SerializeField]private ShotHand shotHand;

	void Update () {
		if(shotHand.shotCount >= 3) {
			this.gameObject.tag = "figure";
		}
	}
}
