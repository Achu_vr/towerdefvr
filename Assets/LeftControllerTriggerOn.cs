﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftControllerTriggerOn : MonoBehaviour {

	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObject;
	[SerializeField]private LeftVibration leftVib;
	public static bool isLeftTriggerOn = false;
	public GameObject attackObj;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
	}

	void Update () {

		if (device.GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
			isLeftTriggerOn = true;
		} else if (device.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) {
			isLeftTriggerOn = false;
			Destroy (attackObj);
		}
	}
}
