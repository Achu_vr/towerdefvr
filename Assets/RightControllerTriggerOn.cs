﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightControllerTriggerOn : MonoBehaviour {

	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObject;
	[SerializeField]private RightVibration rightVib;
	public static bool isRightTriggerOn = false;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
	}

	void Update () {
		if (device.GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
			isRightTriggerOn = true;
		} else if (device.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) {
			isRightTriggerOn = false;
		}
	}
}
