﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeImageSecond : MonoBehaviour {

	private SpriteRenderer ren;
	[SerializeField]private Vector3 nextScale;
	[SerializeField]private Sprite sp;
	[SerializeField]private GameObject textObj;
	void Start () {
		ren = GetComponent<SpriteRenderer> ();
		nextScale.x = 0.24f;
		nextScale.y = 0.33f;

	}

	void Update () {
		if (ren.sprite.name == "cont") {
			this.transform.localScale = nextScale;
			textObj.GetComponent<SpriteRenderer>().sprite = sp;
			Vector3 nextScale2 = new Vector3 (0.18f, 0.3f, 0.21f);
			textObj.transform.localScale = nextScale2;
		}
	}
}
