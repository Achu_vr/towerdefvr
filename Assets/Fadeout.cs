﻿using System.Collections;
using UnityEngine;

public class Fadeout : MonoBehaviour {

	[SerializeField]private SpriteRenderer fadeImage;
	[SerializeField]private float duration = 2.8f;
	public bool isFinished = false;

	[SerializeField]private Fadein fadeinScript;

	void Start () {
		StartCoroutine (FadeImageStart ());	
	}

	public void StartFadeImage () {
		if (fadeinScript.enabled == true) {
			fadeinScript.enabled = false;
		} else if(fadeinScript.enabled == true) {
			fadeinScript.enabled = false;
		}
		StartCoroutine (FadeImage ());
	}

	IEnumerator FadeImage() {
		float timer = 0;
		float alpha = 0f;
		while (fadeImage.color.a <= 1) {
			timer += Time.deltaTime;
			alpha += 0.9f * Time.deltaTime;
			fadeImage.color = new Color (0, 0, 0.1f, alpha);
			yield return null;
		}
		isFinished = true;
	}

	IEnumerator FadeImageStart() {
		float timer = 0;
		float alpha = 0f;
		while (fadeImage.color.a <= 1) {
			timer += Time.deltaTime;
			alpha += 0.25f * Time.deltaTime;
			fadeImage.color = new Color (0, 0, 0, alpha);
			yield return null;
		}
	}
}
