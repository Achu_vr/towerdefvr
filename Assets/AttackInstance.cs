﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackInstance : MonoBehaviour {

	[SerializeField]private int shotCount = 0;

	[SerializeField]private Transform leftArmTrans;
	[SerializeField]private Transform rightArmTrans;
	[SerializeField]private GameObject unityChanAttackPre;

	private Vector3 instancePos;
	private GameObject atkObj;
	[SerializeField]private GameObject player;

	[SerializeField]private LeftControllerTriggerOn lct;
	[SerializeField]private DeleteUnityChanAttack duca;

	private float x, y, z;

	[SerializeField]private NextSprite nextSprite;
	[SerializeField]private ChangeImage changeImage;

	void Update () {

		x = (leftArmTrans.position.x + rightArmTrans.position.x) / 2f;
		y = (leftArmTrans.position.y + rightArmTrans.position.y) / 2f;
		z = (leftArmTrans.position.z + rightArmTrans.position.z) / 2f;

		instancePos = new Vector3 (x, y, z);

		if (duca.canInstantiate) {
			if (LeftControllerTriggerOn.isLeftTriggerOn && RightControllerTriggerOn.isRightTriggerOn) {
				atkObj = Instantiate (unityChanAttackPre, instancePos, player.transform.rotation);
				lct.attackObj = atkObj;
				atkObj.transform.parent = player.transform;
				shotCount++;
			}
		}

		if(shotCount >= 5) {
			nextSprite.ChangeSpriteNext ();
			changeImage.enabled = false;
		}

	}
}
