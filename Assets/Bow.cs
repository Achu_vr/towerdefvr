﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour {

	public bool canShotArrow = false;
	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObject;
	[SerializeField]private LeftVibration leftVib;
	[SerializeField]private GameObject bowObj;

	[SerializeField]private GameObject arrowPre;

	void Start () {
		trackedObject = GetComponent<SteamVR_TrackedObject> ();
		device = SteamVR_Controller.Input ((int)trackedObject.index);
	}

	void Update () {
		if (device.GetPressDown (SteamVR_Controller.ButtonMask.Trigger) && canShotArrow) {
			GameObject arrow = Instantiate (arrowPre, bowObj.transform.position, Quaternion.Inverse(bowObj.transform.rotation));
			arrow.transform.parent = bowObj.transform;
		}
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "bow") {
			canShotArrow = true;
		}
	}

	public void OnTriggerExit() {
		canShotArrow = false;
	}
}
