﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountingMove : MonoBehaviour {

	[SerializeField]private GameObject explainText;
	[SerializeField]private GameObject arrow;
	[SerializeField]private GameObject archer;

	[SerializeField]private GameObject cameraRig;
	private Vector3 lastPos = Vector3.zero;
	private Vector3 nowPos = Vector3.zero;
	public int moveCount = 0;

	[SerializeField]private Material blueMaterial;
	[SerializeField]private Material firstMaterial;

	[SerializeField]private GameObject rightHandModel;
	[SerializeField]private GameObject leftHandModel;

	private GameObject rightObj;
	private GameObject leftObj;


	private bool canNextStep = false;
	private float distance = 0;
	public Text tex;

	[SerializeField]private GameObject rightController;
	[SerializeField]private ShotHand shotHand;
	public bool canPossession = false;
	public bool canLastStep = false;

	IEnumerator WaitOneSecond() {
		yield return new WaitForSeconds (1.5f);
		leftObj  = leftHandModel.transform.FindChild ("trackpad").gameObject;
		firstMaterial = leftObj.GetComponent<MeshRenderer> ().materials [0];
		leftObj.GetComponent<MeshRenderer> ().material  = blueMaterial;
	}

	void Start () {
		lastPos = cameraRig.transform.position;
		tex = explainText.transform.GetChild(0).GetComponent<Text>();
		StartCoroutine ("WaitOneSecond");
	}

	void Update () {

		nowPos = cameraRig.transform.position;
		if (nowPos != lastPos) {
			lastPos = nowPos;
			moveCount++;
			if (moveCount >= 10) {
				leftObj.GetComponent<MeshRenderer> ().material  = firstMaterial;
				tex.text = "足元の矢印の方へ向かいま\nしょう！";
				arrow.SetActive (true);
				archer.SetActive (true);
				canNextStep = true;
			}
		}

		if(shotHand.shotCount >= 3) {
			canPossession = true;
		}

		if (canPossession) {
			tex.text = "キャラクターに手を当てる\nとそのキャラクターに憑依\nできます。してみましょう";
		}

		if (canNextStep) {
			moveCount = 0;
			rightObj = rightHandModel.transform.FindChild ("trigger").gameObject;
			distance = Vector3.Distance (archer.transform.position, cameraRig.transform.position);
			if (distance <= 2.5f && !canPossession && !canLastStep) {
				rightController.GetComponent<ShotHand> ().enabled = true;
				Destroy (arrow);
				rightObj.GetComponent<MeshRenderer> ().material = blueMaterial;
				tex.text = "右トリガーを引くと手元か\nら青い手が飛んでいきます。\n1度飛ばすとしばらく飛ばせ\nません（3回）";
			}

		}

		if (canLastStep) {
			tex.text = "チュートリアル終了\nポータルへいきましょう";
		}
			

	}
}
