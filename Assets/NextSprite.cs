﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class NextSprite : MonoBehaviour {

	[SerializeField]private Sprite sprites;
	[SerializeField]private SpriteRenderer spriteRenderer;

	public void ChangeSpriteNext () {
		spriteRenderer.sprite = sprites;
		StartCoroutine (WaitForTenSec ());
	}

	IEnumerator WaitForTenSec() {
		yield return new WaitForSeconds (10f);
		Destroy (this.gameObject);
	}

}
